﻿using System;

namespace Wojna
{
    class Wojownik:INterfejsDlaWojownika
    {
        public int Atak { get; set; }
        public int Zdrowie { get; set; }
        public string Nazwa { get; set; }

        Random rnd = new Random();
        public Wojownik(string Nazwa, int Zdrowie)
        {
            this.Nazwa = Nazwa;
            this.Zdrowie = Zdrowie;
            Atak = Atak;
        }

        public int LosowyAtak()
        {
            return rnd.Next(1, 101);
        }
  
        public int LosowaTarcza()
        {
            return rnd.Next(41);
        }
    }
}
