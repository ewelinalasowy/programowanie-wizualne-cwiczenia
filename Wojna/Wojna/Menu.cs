﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wojna
{
    class Menu
    {

        public Menu ()
        {
            Wojownik wojownik1 = new Wojownik("Wojownik1", 100);
            Wojownik wojownik2 = new Wojownik("Wojownik2", 100);
            RozpocznijPogram(wojownik1,wojownik2);

        }
        public void WypiszMenu()
        {
            Console.WriteLine("Menu: (wybierz jedną z opcji)");
            Console.WriteLine("1 - nadanie imion wojownikom");
            Console.WriteLine("2 - nadanie zdrowia wojownikom");
            Console.WriteLine("3 - rozpocznij walkę");
            Console.WriteLine("4 - zakończ program");
            Console.WriteLine("Wybór opcji: ");
        }

        public void NadanieImionWojownikom(Wojownik wojownik1, Wojownik wojownik2)
        {
            Console.Write("Podaj imię pierwszego wojownika: ");
            String nazwa = Console.ReadLine();
            wojownik1.Nazwa = nazwa;

            Console.Write("Podaj imię drugiego wojownika: ");
            nazwa = Console.ReadLine();
            wojownik2.Nazwa = nazwa;
        }

        public void NadanieZdrowiaWojownikom(Wojownik wojownik1, Wojownik wojownik2)
        {
            Console.Write("Podaj zdrowie pierwszego wojownika: ");
            int zdrowie = int.Parse(Console.ReadLine());
            wojownik1.Zdrowie = zdrowie;

            Console.Write("Podaj zdrowie drugiego wojownika: ");
            zdrowie = int.Parse(Console.ReadLine());
            wojownik2.Zdrowie = zdrowie;
        }

        public void RozpocznijBitwe(Wojownik wojownik1, Wojownik wojownik2)
        {
            Bitwa.Walka(wojownik1, wojownik2);
        }

        public void RozpocznijPogram(Wojownik wojownik1, Wojownik wojownik2)
        {
            int wybor;

            do
            {
                WypiszMenu();

                wybor = int.Parse(Console.ReadLine());


                switch (wybor)
                {

                    case 1:
                        {
                            NadanieImionWojownikom(wojownik1, wojownik2);

                            break;
                        }

                    case 2:
                        {
                            NadanieZdrowiaWojownikom(wojownik1, wojownik2);

                            break;
                        }

                    case 3:
                        {
                            RozpocznijBitwe(wojownik1, wojownik2);
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

            } while (wybor >= 1 && wybor <= 3);
        }
    }
}
