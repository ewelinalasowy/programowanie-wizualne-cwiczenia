﻿using System;

namespace Wojna
{
    class Bitwa
    {

        public static void Pojedynek(Wojownik wojownik1, Wojownik wojownik2)
        {
            wojownik2.Atak = wojownik2.LosowyAtak();
            int obrazenia = wojownik1.LosowaTarcza() - wojownik2.Atak;

            if (obrazenia > 0)
            {
                obrazenia = 0;
            }
            wojownik1.Zdrowie += obrazenia;
            Console.WriteLine(wojownik2.Nazwa + " zadał " + wojownik1.Nazwa + " " + obrazenia + " obrażeń");
            Console.WriteLine(wojownik1.Nazwa + " ma " + wojownik1.Zdrowie + " życia");

        }

        public static void Walka(Wojownik wojownik1, Wojownik wojownik2)
        {
			
            do

            {
                if (wojownik1.Zdrowie <= 0 || wojownik2.Zdrowie <= 0)
                {
                    Console.WriteLine("Walka nie może się rozpocząć, ponieważ jeden z wojowników nie żyje.");
                    break;
                }


                Pojedynek(wojownik1, wojownik2);


                if (wojownik1.Zdrowie <= 0)
                {
                    Console.WriteLine(wojownik2.Nazwa + " wygrywa");
                    break;
                }

                Pojedynek(wojownik2, wojownik1);

                if (wojownik2.Zdrowie <= 0)
                {
                    Console.WriteLine(wojownik1.Nazwa + " wygrywa");
                    break;
                }

            } while (true);


        }
    }
}

