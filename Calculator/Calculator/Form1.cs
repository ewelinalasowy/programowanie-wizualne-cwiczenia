﻿using System;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        String operation = "";
        double value1 = 0.0;
        double value2 = 0.0;
        Boolean operation_selected;


        public Form1()
        {
            InitializeComponent();
        }

        private void Number(object sender, EventArgs e)
        {
            Button numbers = (Button)sender;
            textBox1.Text += numbers.Text;
        }

        private void Operation(object sender, EventArgs e)
        {
            Button op = (Button)sender;
            operation = op.Text;
            value1 = Double.Parse(textBox1.Text);
            operation_selected = true;
            textBox1.Clear();
            switch (operation)
            {
                case "+":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "-":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "*":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "/":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "%":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "sqrt":

                    lbShowOperation.Text = operation + " " + value1;

                    break;

                case "^":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

                case "mod":

                    lbShowOperation.Text = value1 + " " + operation;

                    break;

            }
        }

        private void Equal(object sender, EventArgs e)
        {
            switch (operation)
            {
                case "+":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = (value1 + value2).ToString();
                    break;
                case "-":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = (value1 - value2).ToString();
                    break;
                case "*":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = (value1 * value2).ToString();
                    break;
                case "/":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = (value1 / value2).ToString();
                    break;

                case "^":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = (Math.Pow(value1, value2)).ToString();
                    break;

                case "%":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = ((value1*value2)/100.0).ToString();
                    break;

                case "sqrt":
                    lbShowOperation.Text =  operation + " " + value1;
                    textBox1.Text = (Math.Sqrt(value1)).ToString();
                    break;

                case "mod":
                    value2 = Double.Parse(textBox1.Text);
                    lbShowOperation.Text = value1 + " " + operation + " " + value2;
                    textBox1.Text = ((value1%value2)).ToString();
                    break;

            }
        }

        private void Dot(object sender, EventArgs e)
        {
            textBox1.Text += ",";
        }

        private void DeleteAll(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void DeleteLast(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
                textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
        }

        private void CloseProgram(object sender, EventArgs e)
        {
            Close();
        }

        private void lbShowOperation_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void plikToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
