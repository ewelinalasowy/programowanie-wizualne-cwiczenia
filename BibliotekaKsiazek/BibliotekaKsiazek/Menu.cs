﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BibliotekaKsiazek
{
    class Menu
    {
        ArrayList options = new ArrayList()
        {
            {""},
            {"Wybierz jedną z opcji:"},
            {"1 - Dodaj nową książkę"},
            {"2 - Usuń książkę o podanym tytule"},
            {"3 - Wyświetl wszystkie książki z mojej biblioteki posortowane wg autorów"},
            {"4 - Wyświetl wszystkie książki z mojej biblioteki posortowane wg tytułów"},
            {"5 - Wyszukaj książkę po tytule" },
            {"6 - Wyszukaj wszystkie książki danego autora"},
            {"7 - Wyczyść bibliotekę"},
            {"8 - Wyjdź z programu"},
        }
       ;

        public Menu()
        {
            SortedDictionary<string, string> library = new SortedDictionary<string, string>()
            {
                {"Wszystkie przygody Sherlocka Holmesa", "Arthur Connan Doyle" },
                {"Lalka", "Bolesław Prus" },
                {"Kamizelka" , "Bolesław Prus" }
            };

            StartProgram(library);

        }

        public void showMenu()
        {
            foreach(string item in options)
            {
                Console.WriteLine(item);
            }
        }



        void StartProgram (SortedDictionary<string,string> library)
        {
            int choose;

            do
            {
                showMenu();
                choose = Int32.Parse(Console.ReadLine());

                switch (choose)
                {
                    case 1:
                        {
                            addBook(library);
                            break;
                        }

                    case 2:
                        {
                            removeBook(library);
                            break;
                        }

                    case 3:
                        {
                            showByAuthors(library);
                            break;
                        }

                    case 4:
                        {
                            showByTitle(library);
                            break;
                        }

                    case 5:
                        {
                            searchByTitle(library);
                            break;
                        }

                    case 6:
                        {
                            searchByAuthor(library);
                            break;
                        }
                    case 7:
                        {
                            clearLibrary(library);
                            break;
                        }
                }

            } while (choose >= 1 && choose <= 7);
               
        }


        void addBook(SortedDictionary<string, string> library)
        {
            Console.WriteLine("Podaj tytuł książki oraz autora");
            library.Add(Console.ReadLine(), Console.ReadLine());
            Console.WriteLine("Pomyślnie dodano książkę do biblioteki");
        }

        void removeBook(SortedDictionary<string, string> library)
        {
            Console.WriteLine("Podaj tytuł książki, którą chcesz usunąć: ");
            string title = Console.ReadLine();

            if (library.ContainsKey(title))
            {
                library.Remove(title);
                Console.WriteLine("Książka została usunięta z biblioteki");
            }
            else
            {
                Console.WriteLine("Książka o podanym tytule nie istnieje");
            }
        }

        void showByAuthors(SortedDictionary<string, string> library)
        {
            foreach (KeyValuePair<string, string> book in library.OrderBy(key => key.Value))

            {
                Console.WriteLine("Tytuł: " + book.Key + "Autor: " + book.Value);
            }
        }

        void showByTitle(SortedDictionary<string, string> library)
        {
            foreach (KeyValuePair<string, string> book in library)
            {
                Console.WriteLine("Tytuł: " + book.Key +" " + " Autor: " + book.Value);
            }
        }

        void searchByTitle(SortedDictionary<string,string> library)
        {
            Console.WriteLine("Podaj tytuł książki, której chcesz wyszukać: ");
            string title = Console.ReadLine();
            if (library.ContainsKey(title))
            {
                Console.WriteLine("Tytuł: " + title + " autor: " + library[title]);
            }
            else Console.WriteLine("Nie ma książki o takim tytule.");
        }

        void searchByAuthor(SortedDictionary<string,string> library)
        {
            Console.WriteLine("Podaj autora, aby wyświetlić wszystkie książki, które zostały przez niego napisane. ");
            string author = Console.ReadLine();

            if (library.ContainsValue(author))
            {
                foreach (KeyValuePair<string, string> book in library)
                {
                    if (book.Value == author)
                        Console.WriteLine("Tytuł: " + book.Key);
                }
            }

            else
            {
                Console.WriteLine("Nie ma takiego autora w bibliotece.");
            }
        }

        void clearLibrary(SortedDictionary<string,string> library)
        {
            library.Clear();
            Console.WriteLine("Biblioteka została wyczyszczona.");
        }

    }
}
