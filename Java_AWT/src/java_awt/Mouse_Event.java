package java_awt;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Mouse_Event extends Frame implements MouseListener {
    Label l;
    Button b;
    Checkbox checkboxMouseClicked;
    Checkbox checkboxMouseEntered;
    Checkbox checkboxMouseExited;
    Checkbox checkboxMousePressed;
    Checkbox checkboxMouseReleased;


    boolean if_paint=false;
    Mouse_Event(){
        addMouseListener(this);
        l=new Label();
        b = new Button("Kliknij mnie!");
        checkboxMouseClicked = new Checkbox("Mouse clicked");
        checkboxMouseEntered = new Checkbox("Mouse entered");
        checkboxMouseExited = new Checkbox("Mouse exited");
        checkboxMousePressed = new Checkbox("Mouse pressed");
        checkboxMouseReleased = new Checkbox("Mouse released");

        l.setBounds(20,50,100,20);
        b.setBounds(130,80,70,20);
        checkboxMouseClicked.setBounds(20,170,100,20);
        checkboxMouseEntered.setBounds(20,190,100,20);
        checkboxMouseExited.setBounds(20,210,100,20);
        checkboxMousePressed.setBounds(20,230,100,20);
        checkboxMouseReleased.setBounds(20,250,100,20);


        b.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent e){

                if_paint=true;
                repaint();

            }
        });
        add(l);
        add(b);
        add(checkboxMouseClicked);
        add(checkboxMouseEntered);
        add(checkboxMouseExited);
        add(checkboxMousePressed);
        add(checkboxMouseReleased);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        setSize(300,300);
        setLayout(null);
        setVisible(true);


    }
    public void mouseClicked(MouseEvent e) {
        l.setText("Mouse Clicked");
        checkboxMouseClicked.setState(true);
    }
    public void mouseEntered(MouseEvent e) {
        l.setText("Mouse Entered");
        checkboxMouseEntered.setState(true);
    }
    public void mouseExited(MouseEvent e) {
        l.setText("Mouse Exited");
        checkboxMouseExited.setState(true);
    }
    public void mousePressed(MouseEvent e) {
        l.setText("Mouse Pressed");
        checkboxMousePressed.setState(true);
    }
    public void mouseReleased(MouseEvent e) {
        l.setText("Mouse Released");
        checkboxMouseReleased.setState(true);
    }

    public void paint(Graphics g) {

        if (if_paint)
        {
            g.setColor( new Color( 255, 20, 10));
            g.fillRect( 130, 110, 70, 50 );
        }


    }


}
